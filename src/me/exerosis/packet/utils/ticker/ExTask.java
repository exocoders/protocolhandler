package me.exerosis.packet.utils.ticker;

import java.util.HashMap;
import java.util.Map;

import me.exerosis.packet.PacketAPI;

import org.bukkit.Bukkit;

public class ExTask {
	public static Map<Runnable, Integer> _ids = new HashMap<Runnable, Integer>();
	
	private ExTask() {}
	
	public static void startTask(Runnable runnable, long delay, long time) {
		if (!isRunning(runnable))
			_ids.put(runnable, Bukkit.getScheduler().runTaskTimer(PacketAPI.getPlugin(), runnable, delay, time).getTaskId());
	}

	public static void stopTask(Runnable runnable) {
		if (isRunning(runnable)) {
			Bukkit.getScheduler().cancelTask(_ids.get(runnable));
			_ids.remove(runnable);
		}
	}

	public static boolean isRunning(Runnable runnable) {
		return Bukkit.getScheduler().isQueued(_ids.get(runnable));
	}
}
package me.exerosis.packet.reflection;

import org.bukkit.Bukkit;

public class Reflect {
	private static final String bukkitClassPrefix = Bukkit.getServer().getClass().getPackage().getName();
	private static final String nmsClassPrefix = bukkitClassPrefix.replace("org.bukkit.craftbukkit", "net.minecraft.server");

	private Reflect() {}

	// Class creators.
	public static ReflectClass<Object> Class(String className) {
		className = className.replace("{cb}", bukkitClassPrefix).replace("{nms}", nmsClassPrefix);

		try {
			java.lang.Class<?> forName = Class.forName(className);
			return new ReflectClass<Object>(forName);
		} catch (Exception ignored) {
			return null;
		}
	}

	public static <T> ReflectClass<T> Class(T instance) {
		return new ReflectClass<T>(instance);
	}

	public static <T> ReflectClass<T> Class(Class<T> clazz) {
		return new ReflectClass<T>(clazz);
	}

	// Field Creators
	public static <K, T> ReflectField<K> Field(int pos, T instance, Class<K> type) {
		return new ReflectClass<T>(instance).getField(type, pos);
	}

	public static <K, T> ReflectField<K> Field(int pos, T instance, ReflectClass<K> type) {
		return new ReflectClass<T>(instance).getField(type, pos);
	}

	public static <K, T> ReflectField<K> Field(String name, T instance, Class<K> type) {
		return new ReflectClass<T>(instance).getField(type, name);
	}

	public static <K, T> ReflectField<K> Field(String name, T instance, ReflectClass<K> type) {
		return new ReflectClass<T>(instance).getField(type, name);
	}

	public static <T> ReflectField<?> Field(String name, T instance) {
		return new ReflectClass<T>(instance).getField(name);
	}

	public static <K, T> ReflectField<K> Field(int pos, Class<T> clazz, Class<K> type) {
		return new ReflectClass<T>(clazz).getField(type, pos);
	}

	public static <K, T> ReflectField<K> Field(int pos, Class<T> clazz, ReflectClass<K> type) {
		return new ReflectClass<T>(clazz).getField(type, pos);
	}

	public static <K, T> ReflectField<K> Field(String name, Class<T> clazz, Class<K> type) {
		return new ReflectClass<T>(clazz).getField(type, name);
	}

	public static <K, T> ReflectField<K> Field(String name, Class<T> clazz, ReflectClass<K> type) {
		return new ReflectClass<T>(clazz).getField(type, name);
	}

	public static <T> ReflectField<?> Field(String name, Class<T> clazz) {
		return new ReflectClass<T>(clazz).getField(name);
	}
}



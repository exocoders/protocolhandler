package me.exerosis.packet.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReflectClass<T> {

	private Class<?> clazz;
	private T instance;
	private ArrayList<ReflectMethod> allMethods = new ArrayList<ReflectMethod>();
	private ArrayList<ReflectField<Object>> allFeilds = new ArrayList<ReflectField<Object>>();
	private ArrayList<Constructor<?>> allConstructors = new ArrayList<Constructor<?>>();

	protected ReflectClass(Class<?> clazz) {
		this.clazz = clazz;
		fillLists();
	}

	protected ReflectClass(T instance) {
		this.instance = instance;
		if(instance != null)
			clazz = instance.getClass();
		fillLists();
	}

	private void fillLists() {
		Constructor<?>[] constructors = clazz.getConstructors();
		if(clazz == null)
			System.out.println("null");
		allConstructors.addAll(Arrays.asList(constructors));
		allConstructors.addAll(Arrays.asList(clazz.getDeclaredConstructors()));

		for(Method method : clazz.getMethods())
			allMethods.add(new ReflectMethod(method));
		for(Field field : clazz.getFields())
			allFeilds.add(new ReflectField<Object>(field, instance));

		Class<?> loopClass = clazz;
		while(loopClass != null) {
			for(Method method : loopClass.getDeclaredMethods())
				allMethods.add(new ReflectMethod(method));
			for(Field field : loopClass.getDeclaredFields())
				allFeilds.add(new ReflectField<Object>(field, instance));
			loopClass = loopClass.getSuperclass();
		}
	}

	//Field getters.
	public <K> ReflectField<K> getField(Class<K> type, int pos) {
		return (ReflectField<K>) getField("", new ReflectClass<K>(type), pos);
	}
	public <K> ReflectField<K> getField(ReflectClass<K> type, int pos) {
		return (ReflectField<K>) getField("", type, pos);
	}
	public <K> ReflectField<K> getField(Class<K> type, String name) {
		return (ReflectField<K>) getField(name, new ReflectClass<K>(type), -1);
	}
	public <K> ReflectField<K> getField(ReflectClass<K> type, String name) {
		return (ReflectField<K>) getField(name, type, -1);
	}
	public ReflectField<Object> getField(String name) {
		for(ReflectField<Object> field : allFeilds)
			if(field.getName().equals(name))
				return field;
		StringBuilder builder = new StringBuilder();
		builder.append("Field not found! \n\tClass: ");
		builder.append(clazz.getSimpleName());
		builder.append("\n\tName: \"");
		builder.append(name);
		throw new RuntimeException(builder.toString());
	}


	@SuppressWarnings("unchecked")
	private <K> ReflectField<K> getField(String name, ReflectClass<K> type, int pos) {
		int index = 0;
		for(ReflectField<Object> field : allFeilds) {
			if(type.getClazz().isAssignableFrom(field.getType())) {
				index++;
				if(pos == -1) {
					if(field.getName().equals(name))
						return (ReflectField<K>) field;
				}
				else if(index == pos)
					return (ReflectField<K>) field;
			}
		}

		StringBuilder builder = new StringBuilder();
		builder.append("Field not found! \n\tClass: ");
		builder.append(clazz.getSimpleName());
		builder.append("\n\tName: \"");
		builder.append(name);
		builder.append("\"\n\tType: ");
		builder.append(type.getClazz().getSimpleName());
		builder.append("\n\tPosition: ");
		builder.append(pos == -1 ? "Not Specified" : pos);
		throw new RuntimeException(builder.toString());
	}


	public Constructor<?> getConstructor(Class<?>... paramTypes) {
		Class<?>[] types = CorrespondingType.getPrimitive(paramTypes);
		for (Constructor<?> constructor : allConstructors) {
			Class<?>[] constructorTypes = CorrespondingType.getPrimitive(constructor.getParameterTypes());
			if (CorrespondingType.compare(types, constructorTypes))
				return constructor;
		}
		StringBuilder builder = new StringBuilder();
		builder.append("Constructor not found!\n\tClass: ");
		builder.append(clazz.getSimpleName());
		builder.append("Params:\n");
		for(Class<?> clazz : types) {
			builder.append(clazz.getSimpleName());
			builder.append("\n");
		}
		throw new RuntimeException(builder.toString());
	}

	@SuppressWarnings("unchecked")
	public Object newInstance(Object... args){
		try {
			instance = (T) getConstructor(CorrespondingType.getPrimitive(args)).newInstance(args);
		}catch(Exception e){e.printStackTrace();}
		return instance;	
	}

	//Method getters.
	public ReflectMethod getMethodByReturn(Class<?> returnType) {
		for (ReflectMethod method: allMethods)
			if (returnType.equals(method.getReturnType()))
				return method;
		throw new RuntimeException("Method not found!");
	}

	public ReflectMethod getMethod(Class<?>... paramTypes) {
		Class<?>[] t = CorrespondingType.getPrimitive(paramTypes);
		for (ReflectMethod method : getMethods()) {
			Class<?>[] types = CorrespondingType.getPrimitive(method.getParameterTypes());
			if (CorrespondingType.compare(types, t))
				return method;
		}
		return null;
	}

	public ReflectMethod getMethod(String name, Class<?>... paramTypes) {
		Class<?>[] t = CorrespondingType.getPrimitive(paramTypes);
		for (ReflectMethod method : getMethods()) {
			Class<?>[] types = CorrespondingType.getPrimitive(method.getParameterTypes());
			if (method.getName().equals(name) && CorrespondingType.compare(types, t))
				return method;
		}
		return null;
	}
	public ReflectMethod getMethod(String name) {
		for (ReflectMethod method : getMethods())
			if(method.getName().equals(name))
				return method;
		return null;
	}

	public boolean isInstance(Object object){
		return clazz.isInstance(object);
	}

	//Fields and methods.
	public List<ReflectMethod> getMethods(){
		List<ReflectMethod> methods = new ArrayList<ReflectMethod>();
		for(Method method : clazz.getMethods())
			methods.add(new ReflectMethod(method, instance));
		return methods;
	}
	public List<ReflectField<Object>> getFields(){
		List<ReflectField<Object>> fields = new ArrayList<ReflectField<Object>>();
		for(Field field : clazz.getFields())
			fields.add(new ReflectField<Object>(field, instance));
		return fields;
	}

	//Declared fields and methods.
	public List<ReflectMethod> getDeclaredMethods(){
		List<ReflectMethod> declaredMethods = new ArrayList<ReflectMethod>();
		for(Method method : clazz.getDeclaredMethods())
			declaredMethods.add(new ReflectMethod(method, instance));
		return declaredMethods;
	}
	public List<ReflectField<Object>> getDeclaredFields(){
		List<ReflectField<Object>> declaredFields = new ArrayList<ReflectField<Object>>();
		for(Field field : clazz.getFields())
			declaredFields.add(new ReflectField<Object>(field, instance));
		return declaredFields;
	}

	//All fields and methods
	public ArrayList<ReflectMethod> getAllMethods() {
		return allMethods;
	}
	public ArrayList<ReflectField<Object>> getAllFeilds() {
		return allFeilds;
	}
	public Object getInstance() {
		return instance;
	}
	public void setInstance(T instance) {
		this.instance = instance;
	}
	public Class<?> getClazz() {
		return clazz;
	}
}

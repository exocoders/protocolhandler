package me.exerosis.packet.reflection;

import java.util.HashMap;
import java.util.Map;

// Utility methods.
public enum CorrespondingType {
	BYTE(byte.class, Byte.class),
	SHORT(short.class, Short.class),
	INTEGER(int.class, Integer.class),
	LONG(long.class, Long.class),
	CHARACTER(char.class, Character.class),
	FLOAT(float.class, Float.class),
	DOUBLE(double.class, Double.class),
	BOOLEAN(boolean.class, Boolean.class);

	private final Class<?> primitive;
	private final Class<?> reference;
	static final Map<Class<?>, CorrespondingType> CORRESPONDING_TYPES = new HashMap<Class<?>, CorrespondingType>();

	static {
		for (CorrespondingType type : CorrespondingType.values()) {
			CORRESPONDING_TYPES.put(type.getPrimitive(), type);
			CORRESPONDING_TYPES.put(type.getReference(), type);
		}
	}
	
	/**
	 * Construct a new data type
	 * 
	 * @param primitive Primitive class of this data type
	 * @param reference Reference class of this data type
	 */
	private CorrespondingType(Class<?> primitive, Class<?> reference) {
		this.primitive = primitive;
		this.reference = reference;
	}
	
	/**
	 * Returns the primitive class of this data type
	 * 
	 * @return The primitive class
	 */
	public Class<?> getPrimitive() {
		return primitive;
	}
	/**
	 * Returns the reference class of this data type
	 * 
	 * @return The reference class
	 */
	public Class<?> getReference() {
		return reference;
	}
	/**
	 * Returns the data type with the given primitive/reference class
	 * 
	 * @param clazz Primitive/Reference class of the data type
	 * @return The data type
	 */
	public static CorrespondingType fromClass(Class<?> clazz) {
		return CORRESPONDING_TYPES.get(clazz);
	}
	
	
	//Utility methods.
	/**
	 * Returns the primitive class of the data type with the given reference class
	 * 
	 * @param clazz Reference class of the data type
	 * @return The primitive class
	 */
	public static Class<?> getPrimitive(Class<?> clazz) {
		CorrespondingType type = fromClass(clazz);
		return type == null ? clazz : type.getPrimitive();
	}
	/**
	 * Returns the primitive class array of the given class array
	 * 
	 * @param classes Given class array
	 * @return The primitive class array
	 */
	public static Class<?>[] getPrimitive(Class<?>[] classes) {
		int length = classes == null ? 0 : classes.length;
		Class<?>[] types = new Class<?>[length];
		for (int index = 0; index < length; index++)
			types[index] = getPrimitive(classes[index]);
		return types;
	}
	/**
	 * Returns the primitive class array of the given object array
	 * 
	 * @param object Given object array
	 * @return The primitive class array
	 */
	public static Class<?>[] getPrimitive(Object[] objects) {
		int length = objects == null ? 0 : objects.length;
		Class<?>[] types = new Class<?>[length];
		for (int index = 0; index < length; index++) {
			Object object = objects[index];
			types[index] = object == null ? null : getPrimitive(object.getClass());
		}
		return types;
	}
	

	/**
	 * Returns the reference class of the data type with the given primitive class
	 * 
	 * @param clazz Primitive class of the data type
	 * @return The reference class
	 */
	public static Class<?> getReference(Class<?> clazz) {
		CorrespondingType type = fromClass(clazz);
		return type == null ? clazz : type.getReference();
	}
	/**
	 * Returns the reference class array of the given class array
	 * 
	 * @param classes Given class array
	 * @return The reference class array
	 */
	public static Class<?>[] getReference(Class<?>[] classes) {
		int length = classes == null ? 0 : classes.length;
		Class<?>[] types = new Class<?>[length];
		for (int index = 0; index < length; index++)
			types[index] = getReference(classes[index]);
		return types;
	}
	/**
	 * Returns the reference class array of the given object array
	 * 
	 * @param object Given object array
	 * @return The reference class array
	 */
	public static Class<?>[] getReference(Object[] objects) {
		int length = objects == null ? 0 : objects.length;
		Class<?>[] types = new Class<?>[length];
		for (int index = 0; index < length; index++)
			types[index] = getReference(objects[index].getClass());
		return types;
	}

	
	/**
	 * Compares two class arrays on equivalence
	 * 
	 * @param primary Primary class array
	 * @param secondary Class array which is compared to the primary array
	 * @return Whether these arrays are equal or not
	 */
	public static boolean compare(Class<?>[] primary, Class<?>[] secondary) {
		if (primary == null || secondary == null || primary.length != secondary.length)
			return false;
		for (int index = 0; index < primary.length; index++) {
			Class<?> primaryClass = primary[index];
			Class<?> secondaryClass = secondary[index];
			if(primaryClass == null && !secondaryClass.equals(Object.class))
				return false;
			if(secondaryClass == null && !primaryClass.equals(Object.class))
				return false;
			if (primaryClass.equals(secondaryClass) || primaryClass.isAssignableFrom(secondaryClass))
				continue;
			return false;
		}
		return true;
	}
	/**
	 * Checks a class is a primitive data type.
	 * @param The class to be checked.
	 * @return Whether it is primitive.
	 */
	public static boolean isPrimative(Class<?> clazz) {
		for (CorrespondingType type : CORRESPONDING_TYPES.values()) {
			if(type.getPrimitive().equals(clazz))
				return true;
		}
		return false;
	}
	/**
	 * Checks a class is a reference type.
	 * @param The class to be checked.
	 * @return Whether it is reference.
	 */
	public static boolean isReference(Class<?> clazz) {
		return !isPrimative(clazz);
	}
}
package me.exerosis.packet.reflection;

import java.lang.reflect.Method;

public class ReflectMethod{
	private Method method;
	private Object _instance;

	public ReflectMethod(Method method) {
		this(method, null);
	}

	public ReflectMethod(Method method, Object instance) {
		this.method = method;
		_instance = instance;
		method.setAccessible(true);
	}

	public Class<?>[] getParameterTypes() {
		return method.getParameterTypes();
	}

	public ReflectClass<Object> getDeclaringClass(){
		return new ReflectClass<Object>(method.getDeclaringClass());
	}

	public ReflectClass<Object> getReturnClass(){
		return new ReflectClass<Object>(method.getReturnType());
	}
	
	public void setInstance(Object instance) {
		_instance = instance;
	}

	public Object call(Object... params) {
		try{
			return method.invoke(_instance, params);
		} catch (Exception exception) {
			throw new RuntimeException(exception);
		}
	}

	public String getName() {
		return method.getName();
	}

	public Class<?> getReturnType() {
		return method.getReturnType();
	}
}
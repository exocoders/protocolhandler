package me.exerosis.packet.player.injection.packets.entity.npc;

public enum Pose {
	SLEEPING, SNEAKING, SPRINTING;
}

package me.exerosis.packet.player.injection.packets;

import me.exerosis.packet.player.injection.packet.player.PacketPlayer;
import me.exerosis.packet.reflection.Reflect;
import me.exerosis.packet.reflection.ReflectClass;

public class Animation implements PreconPacket {
	private ReflectClass<Object> packet;
	
	public Animation(int entityId, int animationId) {
		packet = Reflect.Class("{nms}PacketPlayOutAnimation");
		packet.newInstance();
		Reflect.Field("a", packet, int.class).setValue(entityId);
		Reflect.Field("b", packet, int.class).setValue(animationId);
	}

	public void send(PacketPlayer player) {
		player.sendPacket(packet.getInstance());
	}
}

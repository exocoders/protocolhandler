package me.exerosis.packet.player.injection.packets.entities;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;

import me.exerosis.packet.PacketAPI;
import me.exerosis.packet.player.injection.events.PacketEventInUseEntity;
import me.exerosis.packet.player.injection.packet.player.PacketPlayer;
import me.exerosis.packet.player.injection.packet.player.handlers.PlayerHandler;
import net.minecraft.server.v1_8_R1.Packet;
import net.minecraft.server.v1_8_R1.PacketPlayOutEntityDestroy;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public abstract class Entity implements Listener, Serializable{
	private static final long serialVersionUID = 8606871941638844968L;

	private String name;
	private Location location;

	public Entity(String name, Location location) {
		this.name = name;
		this.location = location;
		Bukkit.getPluginManager().registerEvents(this, PacketAPI.getPlugin());
	}		

	/**
	 * Gets the command used to create and show the NPC to a player, allows for player modifications as well.
	 */
	public abstract String getSpawnCommand(PacketPlayer player);
	/**
	 * Gets the NPCs entity ID.
	 */
	public abstract int[] getEID();
	/**
	 * Called when this NPC is clicked on.
	 */
	@EventHandler
	public void onClick(PacketEventInUseEntity event) {};


	public String getSpawnCommand(Player player) {
		return getSpawnCommand(PlayerHandler.getPlayer(player));
	}

	/**
	 * Returns the packet that destroys the entity.
	 * @return
	 */
	public Packet getRemovePacket() {
		return new PacketPlayOutEntityDestroy(getEID());
	}


	/**				
	 * Send a modification to the NPC that only one player can see and will not be saved by the server. 
	 * Input a unique string part of a "getPacket" method.
	 * @param command
	 * @param objects
	 */
	public boolean sendCommand(String command, PacketPlayer player, Object... objects) {
		outer : for(String cmd : command.split(", ")) {
			Class<?> clazz = this.getClass();
			for(Method method : clazz.getMethods()) {
				if(!method.getName().startsWith("get") || !method.getName().contains("Packet") || !method.getName().contains(cmd))
					continue;
				try {
					Packet packet = (Packet) method.invoke(this, objects);
					if(player != null)
						player.sendPacket(packet);
					for(PacketPlayer onlinePlayer : PlayerHandler.getOnlinePlayers())
						onlinePlayer.sendPacket(packet);
					continue outer;
				} catch (Exception e) {e.printStackTrace();}
			}
		}
	return false;
	}

	/**
	 * Makes sure a player can see all surrounding players
	 * @param player
	 */
	public static void refreshPlayer(final Player player) {
		final int visibleDistance = Bukkit.getServer().getViewDistance() * 16;
		Bukkit.getScheduler().runTaskLater(PacketAPI.getPlugin(), new Runnable() {
			
			@SuppressWarnings("deprecation")
			@Override
			public void run() {
				final Set<Player> players = new HashSet<Player>();
				for (Player otherPlayer : Bukkit.getOnlinePlayers()) {
					if (otherPlayer.getLocation().distance(player.getLocation()) > visibleDistance)
						continue;

					if (!otherPlayer.canSee(player))
						continue;
					
					players.add(otherPlayer);
					otherPlayer.hidePlayer(player);	
				}

				Bukkit.getScheduler().runTaskLater(PacketAPI.getPlugin(), new Runnable() {
					@Override
					public void run() {
						for (Player otherPlayer : players) {
							if (otherPlayer == null || !otherPlayer.isValid() || player == null || !player.isValid())
								return;
							otherPlayer.showPlayer(player);
						}
					}
				}, 1);
			}
		}, 2);
	}

	/**
	 * Saves the NPC data to a .dat.
	 */
	public void save(){	
		try {
			File file = new File("NPC Data");

			if(!file.exists()){
				file.mkdirs();
				System.out.println("[NPCs] Created 'NPC Data' folder.");
			}

			file = new File("NPC Data/" + name + ".dat");

			file.createNewFile();
			System.out.println("[NPCs] Created '" + name + "' npc data file.");


			FileOutputStream fileOutputStream = new FileOutputStream(file);
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
			objectOutputStream.writeObject(this);
			objectOutputStream.close();
		}
		catch (Exception exception) {exception.printStackTrace();}
	}


	//Events	
	@EventHandler
	private void onPlayerJoinEvent(PlayerJoinEvent event){
		sendModCommand(getSpawnCommand(event.getPlayer()), event.getPlayer());
	}

	@EventHandler
	private void onClickEvent(PacketEventInUseEntity event){
		if(event.getTargetId() == getEID()[0])
			onClick(event);	
	}



	//Getters.
	/**
	 * Returns the NPC's location.
	 * @return
	 */
	public Location getLocation() {
		return location;
	}
	/**
	 * Returns the NPC's name.
	 * @return
	 */
	public String getName() {
		return name;
	}
	
	
	/**				
	 * Send a modification to the NPC that only one player can see and will not be saved by the server. 
	 * Input a unique string part of a "getPacket" method.
	 * @param command
	 * @param objects
	 */
	public boolean sendModCommand(String command, Object... objects) {
		return sendCommand(command, null, objects);
	}
	/**				
	 * Send a modification to the NPC that only one player can see and will not be saved by the server. 
	 * Input a unique string part of a "getPacket" method.
	 * @param command
	 * @param objects
	 */
	public boolean sendModCommand(String command) {
		return sendCommand(command, null, new Object[] {});
	}
	/**				
	 * Send a modification to the NPC that only one player can see and will not be saved by the server. 
	 * Input a unique string part of a "getPacket" method.
	 * @param command
	 * @param objects
	 */
	public boolean sendModCommand(String command, Player player, Object... objects) {
		return sendCommand(command, PlayerHandler.getPlayer(player), objects);
	}
	/**				
	 * Send a modification to the NPC that only one player can see and will not be saved by the server. 
	 * Input a unique string part of a "getPacket" method.
	 * @param command
	 * @param objects
	 */
	public boolean sendModCommand(String command, PacketPlayer player, Object... objects) {
		return sendCommand(command, player, objects);
	}
	/**				
	 * Send a modification to the NPC that only one player can see and will not be saved by the server. 
	 * Input a unique string part of a "getPacket" method.
	 * @param command
	 * @param objects
	 */
	public boolean sendModCommand(String command, Player player) {
		return sendCommand(command, PlayerHandler.getPlayer(player), new Object[] {});
	}
	/**				
	 * Send a modification to the NPC that only one player can see and will not be saved by the server. 
	 * Input a unique string part of a "getPacket" method.
	 * @param command
	 * @param objects
	 */
	public boolean sendModCommand(String command, PacketPlayer player) {
		return sendCommand(command, player, new Object[] {});
	}

}
package me.exerosis.packet.player.injection.packets;

import java.util.UUID;

import me.exerosis.packet.player.injection.packet.player.PacketPlayer;
import me.exerosis.packet.reflection.Reflect;
import net.minecraft.server.v1_8_R1.DataWatcher;
import net.minecraft.server.v1_8_R1.MathHelper;
import net.minecraft.server.v1_8_R1.PacketPlayOutSpawnEntityLiving;

import org.bukkit.Location;

public class SpawnEntity implements PreconPacket{
	private PacketPlayOutSpawnEntityLiving packet;
	
	public SpawnEntity(int id, UUID uuid, Location location, int itemID, DataWatcher whatcher) {
		packet = new PacketPlayOutSpawnEntityLiving();
		Reflect.Field("a", packet, int.class).setValue(id);
		Reflect.Field("b", packet, UUID.class).setValue(uuid);
		Reflect.Field("c", packet, int.class).setValue((int)(MathHelper.floor(location.getX()* 32.0D)));
		Reflect.Field("d", packet, int.class).setValue((int)(MathHelper.floor(location.getY()* 32.0D)));
		Reflect.Field("e", packet, int.class).setValue((int)(MathHelper.floor(location.getZ()* 32.0D)));
		Reflect.Field("f", packet, int.class).setValue((int)(location.getYaw() * 256.0F / 360.0F));
		Reflect.Field("g", packet, int.class).setValue((int)(location.getPitch() * 256.0F / 360.0F));
		Reflect.Field("h", packet, int.class).setValue(itemID);
		Reflect.Field("i", packet, DataWatcher.class).setValue(whatcher);
	}
	
	public void send(PacketPlayer player) {
		player.sendPacket(packet);
	}
}
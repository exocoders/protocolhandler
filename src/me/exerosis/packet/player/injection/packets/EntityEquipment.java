package me.exerosis.packet.player.injection.packets;

import me.exerosis.packet.player.injection.packet.player.PacketPlayer;
import me.exerosis.packet.reflection.Reflect;

import org.bukkit.inventory.ItemStack;

public class EntityEquipment implements PreconPacket{
	private Object packet;

	public EntityEquipment(int EID, int slot, ItemStack itemStack) {
		Object item = Reflect.Class("{cb}.inventory.CraftItemStack").getMethod("asNMSCopy").call(itemStack);
		packet = Reflect.Class("{nms}.PacketPlayOutEntityEquipment").newInstance(EID, slot, item);
	}

	public void send(PacketPlayer player) {
		player.sendPacket(packet);
	}
}

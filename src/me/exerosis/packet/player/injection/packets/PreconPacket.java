package me.exerosis.packet.player.injection.packets;

import me.exerosis.packet.player.injection.packet.player.PacketPlayer;

public interface PreconPacket {
	public void send(PacketPlayer player);
}

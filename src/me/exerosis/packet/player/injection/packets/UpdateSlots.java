package me.exerosis.packet.player.injection.packets;

import java.util.List;

import me.exerosis.packet.player.injection.packet.player.PacketPlayer;
import me.exerosis.packet.reflection.Reflect;

public class UpdateSlots implements PreconPacket{
	private Object packet;
	
	public UpdateSlots(List<Object> itemStacks) {
		packet = Reflect.Class("{nms}PacketPlayOutWindowItems").newInstance(0, itemStacks);
	}

	public void send(PacketPlayer player) {
		player.sendPacket(packet);	
	}	
}

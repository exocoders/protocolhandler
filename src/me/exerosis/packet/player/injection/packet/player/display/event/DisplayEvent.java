package me.exerosis.packet.player.injection.packet.player.display.event;

import me.exerosis.packet.player.injection.packet.player.display.Displayable;

import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class DisplayEvent extends Event implements Cancellable {
	private boolean _cancelled;
	private Displayable _displayable;
	
	public DisplayEvent(Displayable displayable) {
		_displayable = displayable;
	}

	public Displayable getDisplayable() {
		return _displayable;
	}
	
	@Override
	public boolean isCancelled() {
		return _cancelled;
	}

	@Override
	public void setCancelled(boolean arg0) {
		_cancelled = arg0;
	}
	
	private static final HandlerList handlers = new HandlerList();
	public HandlerList getHandlers() {return handlers;}
	public static HandlerList getHandlerList() {return handlers;}
}
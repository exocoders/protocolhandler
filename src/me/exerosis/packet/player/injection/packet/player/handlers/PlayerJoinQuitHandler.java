package me.exerosis.packet.player.injection.packet.player.handlers;

import java.util.Collection;
import java.util.HashMap;

import me.exerosis.packet.PacketAPI;
import me.exerosis.packet.player.injection.packet.player.PacketPlayer;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.server.PluginDisableEvent;
import org.bukkit.event.server.PluginEnableEvent;

@SuppressWarnings("deprecation")
public class PlayerJoinQuitHandler implements Listener{
	protected static HashMap<Player, PacketPlayer> packetPlayers = new HashMap<Player, PacketPlayer>();

	protected PlayerJoinQuitHandler() {
		Bukkit.getPluginManager().registerEvents(this, PacketAPI.getPlugin());
	}

	/**
	 * Gets an online PacketPlayer.
	 * @param player
	 * @return packetPlayer
	 */
	public static PacketPlayer getPlayer(Player player) {
		return packetPlayers.get(player);
	}

	/**
	 * Gets every online PacketPlayer.
	 * @param player
	 * @return packetPlayers
	 */
	public static Collection<PacketPlayer> getOnlinePlayers() {
		return packetPlayers.values();
	}

	@SuppressWarnings("static-method")
	@EventHandler
	public void onEnable(PluginEnableEvent event){
		if(!event.getPlugin().equals(PacketAPI.getPlugin()))
			return;
		for(Player player : Bukkit.getOnlinePlayers()){
			packetPlayers.put(player, new PacketPlayer(player));
		}
	}

	@SuppressWarnings("static-method")
	@EventHandler
	public void onDisable(PluginDisableEvent event){
		if(!event.getPlugin().equals(PacketAPI.getPlugin()))
			return;
		for(Player onlinePlayer : Bukkit.getOnlinePlayers()){	
			PacketPlayer player = packetPlayers.remove(onlinePlayer);
			player.unInject();
		}
	}

	@SuppressWarnings("static-method")
	@EventHandler
	public void onJoin(PlayerJoinEvent event){
		packetPlayers.put(event.getPlayer(), new PacketPlayer(event.getPlayer()));
	}

	@SuppressWarnings("static-method")
	@EventHandler
	public void onQuit(PlayerQuitEvent event){
		PacketPlayer player = packetPlayers.remove(event.getPlayer());
		player.unInject();
	}
}

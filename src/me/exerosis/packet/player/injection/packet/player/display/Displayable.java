package me.exerosis.packet.player.injection.packet.player.display;

import me.exerosis.packet.player.injection.packet.player.PacketPlayer;

public abstract class Displayable implements Comparable<Displayable> {
	private int _priority;

	public Displayable(int priority) {
		_priority = priority;
	}
	
	public void show(PacketPlayer player) {}
	public void hide(PacketPlayer player) {}
	
	public int compareTo(Displayable o) {
		return Integer.compare(_priority, o.getPriority());
	}
	
	public int getPriority() {
		return _priority;
	}
}
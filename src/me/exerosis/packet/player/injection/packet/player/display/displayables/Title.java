package me.exerosis.packet.player.injection.packet.player.display.displayables;

import me.exerosis.packet.PacketAPI;
import me.exerosis.packet.player.injection.packet.player.PacketPlayer;
import me.exerosis.packet.player.injection.packet.player.display.DisplayUtils;
import me.exerosis.packet.player.injection.packet.player.display.Displayable;
import net.minecraft.server.v1_8_R1.EnumTitleAction;
import net.minecraft.server.v1_8_R1.Packet;
import net.minecraft.server.v1_8_R1.PacketPlayOutTitle;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.Listener;

public class Title extends Displayable implements Listener {
	private String title = "";
	private ChatColor titleColor = ChatColor.WHITE;

	private String subtitle = "";
	private ChatColor subtitleColor = ChatColor.WHITE;

	private int fadeInTime, stayTime, fadeOutTime;
	private boolean ticks = false;

	/**
	 * Create a new title
	 * 
	 * @param title
	 *            Title
	 */
	public Title(int priority, String title) {
		this(priority, title, "");
		this.title = title;
	}

	/**
	 * Create a new title
	 * 
	 * @param title
	 *            Title text
	 * @param subtitle
	 *            Subtitle text
	 */
	public Title(int priority, String title, String subtitle) {
		this(priority, title, subtitle, -1, -1, -1);
		this.title = title;
		this.subtitle = subtitle;
	}

	/**
	 * Create a new title
	 * 
	 * @param title
	 *            Title text
	 * @param subtitle
	 *            Subtitle text
	 * @param fadeInTime
	 *            Fade in time
	 * @param stayTime
	 *            Stay on screen time
	 * @param fadeOutTime
	 *            Fade out time
	 */
	public Title(int priority, String title, String subtitle, int fadeInTime, int stayTime, int fadeOutTime) {
		super(priority);
		this.title = title;
		this.subtitle = subtitle;
		this.fadeInTime = fadeInTime;
		this.stayTime = stayTime;
		this.fadeOutTime = fadeOutTime;
		Bukkit.getPluginManager().registerEvents(this, PacketAPI.getPlugin());
	}

	/**
	 * Copy title
	 * 
	 * @param title
	 *            Title
	 */
	public Title(int priority, Title title) {
		this(priority, title.getTitle(), title.getSubtitle(), title.getFadeInTime(), title.getStayTime(), title.getFadeOutTime());
		this.titleColor = title.getTitleColor();
		this.subtitleColor = title.getSubtitleColor();
		this.ticks = title.isInTicks();
	}

	
	public Packet getTimePacket() {
		return new PacketPlayOutTitle(EnumTitleAction.TIMES, null, getFadeInTime(), getStayTime(), getFadeOutTime());
	}
	public Packet getTitlePacket() {
		return new PacketPlayOutTitle(EnumTitleAction.TITLE, DisplayUtils.serializeText(title, titleColor));
	}
	public Packet getSubtitlePacket() {
		return new PacketPlayOutTitle(EnumTitleAction.SUBTITLE, DisplayUtils.serializeText(subtitle, subtitleColor));
	}
	public static Packet getClearPacket(){
		return new PacketPlayOutTitle(EnumTitleAction.CLEAR, null);
	}
	public static Packet getResetPacket(){
		return new PacketPlayOutTitle(EnumTitleAction.RESET, null);
	}

	
	@Override
	public void show(PacketPlayer player) {
		player.sendPacket(getResetPacket());
		refreshTitle(player);
		Bukkit.getScheduler().runTaskLater(PacketAPI.getPlugin(), new Runnable() {
			@Override
			public void run() {
				player.getDisplayHolder(Title.class).remove(Title.this);
			}
		}, (fadeInTime + fadeOutTime + stayTime) * (ticks ? 1 : 20));
	}
	
	@Override
	public void hide(PacketPlayer player) {
		player.sendPacket(getClearPacket());
	}

	public void refreshTitle(PacketPlayer player) {
		if (fadeInTime != -1 && fadeOutTime != -1 && stayTime != -1)
			player.sendPacket(getTimePacket());
		if(!title.equals(""))
			player.sendPacket(getTitlePacket());
		if(!subtitle.equals(""))
			player.sendPacket(getSubtitlePacket());
	}

	/**
	 * Get fade in time
	 * @return fade in time
	 */
	public int getFadeInTime() {
		return fadeInTime * (ticks ? 1 : 20);
	}

	/**
	 * Get stay time
	 * @return stay time
	 */
	public int getStayTime() {
		return stayTime * (ticks ? 1 : 20);
	}

	/**
	 * Get fade out time
	 * @return fade out time
	 */
	public int getFadeOutTime() {
		return fadeOutTime * (ticks ? 1 : 20);
	}

	/**
	 * Set title text
	 * 
	 * @param title
	 *            Title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Get title text
	 * 
	 * @return Title text
	 */
	public String getTitle() {
		return this.title;
	}

	/**
	 * Set subtitle text
	 * 
	 * @param subtitle
	 *            Subtitle text
	 */
	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	/**
	 * Get subtitle text
	 * 
	 * @return Subtitle text
	 */
	public String getSubtitle() {
		return this.subtitle;
	}

	/**
	 * Set the title color
	 * 
	 * @param color
	 *            Chat color
	 */
	public void setTitleColor(ChatColor color) {
		this.titleColor = color;
	}

	/**
	 * Get the title color
	 * 
	 * @return tile color 
	 */
	public ChatColor getTitleColor() {
		return titleColor;
	}

	/**
	 * Set the subtitle color
	 * 
	 * @param color
	 *            Chat color
	 */
	public void setSubtitleColor(ChatColor color) {
		this.subtitleColor = color;
	}

	/**
	 * Get the subtitle color
	 * 
	 * @return subtitle color 
	 */
	public ChatColor getSubtitleColor() {
		return subtitleColor;
	}

	/**
	 * Set title fade in time
	 * 
	 * @param time
	 *            Time
	 */
	public void setFadeInTime(int time) {
		this.fadeInTime = time;
	}

	/**
	 * Set title fade out time
	 * 
	 * @param time
	 *            Time
	 */
	public void setFadeOutTime(int time) {
		this.fadeOutTime = time;
	}

	/**
	 * Set title stay time
	 * 
	 * @param time
	 *            Time
	 */
	public void setStayTime(int time) {
		this.stayTime = time;
	}

	/**
	 * Set timings to ticks
	 */
	public void setInTicks(boolean inTicks) {
		ticks = inTicks;
	}
	/**
	 * Check if the timings are in ticks.
	 * @return
	 */
	public boolean isInTicks() {
		return ticks;
	}
}
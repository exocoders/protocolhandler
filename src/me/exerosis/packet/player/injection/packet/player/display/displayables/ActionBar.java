package me.exerosis.packet.player.injection.packet.player.display.displayables;

import java.util.HashSet;
import java.util.Set;

import me.exerosis.packet.player.injection.packet.player.PacketPlayer;
import me.exerosis.packet.player.injection.packet.player.display.DisplayUtils;
import me.exerosis.packet.player.injection.packet.player.display.Displayable;
import me.exerosis.packet.utils.ticker.ExTask;
import net.minecraft.server.v1_8_R1.PacketPlayOutChat;

import org.bukkit.ChatColor;

public class ActionBar extends Displayable implements Runnable {
	private String message;
	private Set<PacketPlayer> _players = new HashSet<PacketPlayer>();

	public ActionBar(int priority, String message) {
		super(priority);
		this.message = message;
	}	

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
	
	@Override
	public void show(PacketPlayer player) {
		ExTask.startTask(this, 1, 1);
		_players.add(player);
	}
	
	@Override
	public void hide(PacketPlayer player) {
		ExTask.startTask(this, 1, 1);
		_players.remove(player);
	}

	@Override
	public void run() {
		for (PacketPlayer player : _players)
			player.sendPacket(new PacketPlayOutChat(DisplayUtils.serializeText(message, ChatColor.WHITE), (byte) 2));
	}
}
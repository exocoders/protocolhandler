package me.exerosis.packet.player.injection.packet.player;

import io.netty.channel.Channel;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;

import java.lang.reflect.Field;
import java.util.NoSuchElementException;
import java.util.concurrent.Future;

import me.exerosis.packet.PacketAPI;
import me.exerosis.packet.player.injection.events.PacketEvent;
import me.exerosis.packet.player.injection.events.PacketEventFireTask;
import me.exerosis.packet.player.injection.events.PacketEventInChat;
import me.exerosis.packet.player.injection.events.PacketEventInResourcePackStatus;
import me.exerosis.packet.player.injection.events.PacketEventInUseEntity;
import me.exerosis.packet.player.injection.events.PacketEventInWindowClick;
import me.exerosis.packet.player.injection.events.PacketEventInWindowClose;
import me.exerosis.packet.player.injection.events.PacketEventOutConfirmTransaction;
import me.exerosis.packet.player.injection.events.PacketEventOutEntityEffect;
import me.exerosis.packet.player.injection.events.PacketEventOutEntityEquipment;
import me.exerosis.packet.player.injection.events.PacketEventOutResourcePackSend;
import me.exerosis.packet.player.injection.events.PacketEventOutSetSlot;
import me.exerosis.packet.player.injection.events.PacketEventOutUpdateTime;
import net.minecraft.server.v1_8_R1.NetworkManager;
import net.minecraft.server.v1_8_R1.Packet;
import net.minecraft.server.v1_8_R1.PacketPlayInChat;
import net.minecraft.server.v1_8_R1.PacketPlayInCloseWindow;
import net.minecraft.server.v1_8_R1.PacketPlayInFlying;
import net.minecraft.server.v1_8_R1.PacketPlayInLook;
import net.minecraft.server.v1_8_R1.PacketPlayInResourcePackStatus;
import net.minecraft.server.v1_8_R1.PacketPlayInUseEntity;
import net.minecraft.server.v1_8_R1.PacketPlayInWindowClick;
import net.minecraft.server.v1_8_R1.PacketPlayOutEntityEffect;
import net.minecraft.server.v1_8_R1.PacketPlayOutEntityEquipment;
import net.minecraft.server.v1_8_R1.PacketPlayOutResourcePackSend;
import net.minecraft.server.v1_8_R1.PacketPlayOutSetSlot;
import net.minecraft.server.v1_8_R1.PacketPlayOutTransaction;
import net.minecraft.server.v1_8_R1.PacketPlayOutUpdateTime;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.google.common.base.Preconditions;

public class PlayerInjector extends ChannelDuplexHandler {
	private static Field CHANNEL_FIELD;

	//Player fields
	private PacketPlayer packetPlayer;
	private Player player;

	//Channel
	private Channel channel;

	//Status
	private boolean isInjected;
	private boolean isOpen;

	protected PlayerInjector(PacketPlayer packetPlayer) {
		Preconditions.checkNotNull(packetPlayer, "Player cannot be NULL!");
		this.packetPlayer = packetPlayer;
		player = packetPlayer.getPlayer();
	}

	protected void unInject() {
		if(!isOpen || !isInjected)
			return;
		channel.eventLoop().execute(new Runnable() {

			@Override
			public void run() {
				try {
					//channel.pipeline().remove(PlayerInjector.this);
					channel.pipeline().remove("custom_packet_handler");
				} catch (NoSuchElementException e) {}
			}
		});

		isOpen = false;
		isInjected = false;
	}

	protected void inject() {
		initialize();

		synchronized (packetPlayer.getNetworkManager()) {
			if (isInjected || isOpen)
				return;
			if(channel == null)
				throw new IllegalStateException("Channel is NULL!");

			try {
				if(channel.pipeline().get("custom_packet_handler") != null)
					channel.pipeline().remove("custom_packet_handler");
				channel.pipeline().addBefore("packet_handler", "custom_packet_handler", this);
			} catch (NoSuchElementException e){}
			catch(IllegalArgumentException exception){
				Bukkit.broadcastMessage(ChatColor.RED + "Duplicate handler name: custom_packet_handler");
			}

			isInjected = true;
			isOpen = true;
		}
	}

	protected void sendPacket(Object packet) {
		if(!isOpen)
			throw new IllegalStateException("PlayerInjector is closed!");
		channel.writeAndFlush(packet);
	}

	protected void receivePacket(Object packet) {
		if(!isOpen)
			throw new IllegalStateException("PlayerInjector is closed!");
		channel.pipeline().context("encoder").fireChannelRead(packet);
	}

	protected boolean isInjected() {
		return isInjected;
	}

	protected boolean isOpen() {
		return isOpen;
	}

	protected PacketPlayer getPlayer() {
		return packetPlayer;
	}


	//Private methods for handling in initialization and event firing.
	private static void initializeFields() {
		if (CHANNEL_FIELD != null)
			return;
		try {
			CHANNEL_FIELD = NetworkManager.class.getDeclaredField("i");
			CHANNEL_FIELD.setAccessible(true);
		} catch (Exception e) {}
		if(CHANNEL_FIELD == null)
			throw new IllegalStateException("Channel is NULL! \n Shutting down the server, is PlayerInjector up to date? (PlayerInjector version: 1.8.1)");
	}	

	private void initialize() {
		initializeFields();
		try {
			channel = (Channel) CHANNEL_FIELD.get(packetPlayer.getNetworkManager());
		} catch (Exception e) {
			throw new RuntimeException("Failed to get the channel for player: " + player.getName(), e);
		}	
	}

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object packet) throws Exception {
		if (!(packet instanceof Packet))
			super.channelRead(ctx, packet);
		else if(packet instanceof PacketPlayInFlying)
			super.channelRead(ctx, packet);
		else if(packet instanceof PacketPlayInLook)
			super.channelRead(ctx, packet);
		else if (packet instanceof PacketPlayInChat)
			firePacketEvent(ctx, new PacketEventInChat((PacketPlayInChat) packet, packetPlayer));
		else if (packet instanceof PacketPlayInWindowClick)
			firePacketEvent(ctx, new PacketEventInWindowClick((PacketPlayInWindowClick) packet, packetPlayer));
		else if (packet instanceof PacketPlayInResourcePackStatus)
			firePacketEvent(ctx, new PacketEventInResourcePackStatus((PacketPlayInResourcePackStatus) packet, packetPlayer));
		else if (packet instanceof PacketPlayInUseEntity)
			firePacketEvent(ctx, new PacketEventInUseEntity((PacketPlayInUseEntity) packet, packetPlayer));
		else if (packet instanceof PacketPlayInCloseWindow)
			firePacketEvent(ctx, new PacketEventInWindowClose((PacketPlayInCloseWindow) packet, packetPlayer));
		else
		super.channelRead(ctx, packet);

	}

	@Override
	public void write(ChannelHandlerContext ctx, Object packet, ChannelPromise promise) throws Exception {
		if (packet instanceof PacketPlayOutEntityEffect)
			firePacketEvent(ctx, new PacketEventOutEntityEffect(packet, packetPlayer), promise);
		else if (packet instanceof PacketPlayOutResourcePackSend)
			firePacketEvent(ctx, new PacketEventOutResourcePackSend(packet, packetPlayer), promise);
		else if (packet instanceof PacketPlayOutTransaction)
			firePacketEvent(ctx, new PacketEventOutConfirmTransaction(packet, packetPlayer), promise);
		else if (packet instanceof PacketPlayOutUpdateTime)
			firePacketEvent(ctx, new PacketEventOutUpdateTime(packet, packetPlayer), promise);
		else if (packet instanceof PacketPlayOutEntityEquipment)
			firePacketEvent(ctx, new PacketEventOutEntityEquipment(packet, packetPlayer), promise);
		else if (packet instanceof PacketPlayOutSetSlot)
			firePacketEvent(ctx, new PacketEventOutSetSlot(packet, packetPlayer), promise);
		else
			super.write(ctx, packet, promise);
	}

	private void firePacketEvent(ChannelHandlerContext ctx, PacketEvent event) {
		firePacketEvent(ctx, event, null);
	}

	private void firePacketEvent(ChannelHandlerContext ctx, PacketEvent event, ChannelPromise promise) {
		if (event.isAsynchronous())
			Bukkit.getPluginManager().callEvent(event);
		else {
			
			Future<PacketEvent> futureTask = Bukkit.getScheduler().callSyncMethod(PacketAPI.getPlugin(), new PacketEventFireTask(event));

			try {
				event = (PacketEvent) futureTask.get();
			} catch (Exception e) {e.printStackTrace();}
		}


		if (event.isCancelled())
			return;
		try {
			if (promise == null) super.channelRead(ctx, event.getPacket()); 
			else super.write(ctx, event.getPacket(), promise);
		} catch (Exception e) {e.printStackTrace();}
	}
}
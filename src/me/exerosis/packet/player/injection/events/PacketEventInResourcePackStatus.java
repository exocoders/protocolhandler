package me.exerosis.packet.player.injection.events;

import me.exerosis.packet.player.injection.packet.player.PacketPlayer;
import me.exerosis.packet.reflection.Reflect;

public class PacketEventInResourcePackStatus extends PacketEvent{
	private String hash;
	private int status;
	
	@SuppressWarnings("rawtypes")
	public PacketEventInResourcePackStatus(Object packet, PacketPlayer player) {
		super(packet, player);
		hash = Reflect.Field("a", packet, String.class).getValue();
		status = ((Enum) Reflect.Field("b", packet, Object.class).getValue()).ordinal();
	}
	
	public String getHash() {
		return hash;
	}
	public int getStatus() {
		return status;
	}
	public void setHash(String hash) {
		this.hash = hash;
		Reflect.Field("a", super.getPacket(), String.class).setValue(hash);
	}
	public void setStatus(int status) {
		this.status = status;
		Reflect.Field("b", super.getPacket(), int.class).setValue(status);
	}
}

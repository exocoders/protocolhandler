package me.exerosis.packet.player.injection.events;

import me.exerosis.packet.player.injection.packet.player.PacketPlayer;
import me.exerosis.packet.reflection.Reflect;

public class PacketEventOutResourcePackSend extends PacketEvent{
	private String URL;
	private String hash;

	public PacketEventOutResourcePackSend(Object packet, PacketPlayer player) {
		super(packet, player);
		URL = Reflect.Field("a", packet, String.class).getValue();
		hash = Reflect.Field("b", packet, String.class).getValue();
	}
	public String getURL() {
		return URL;
	}
	public String getHash() {
		return hash;
	}
	public void setURL(String URL) {
		Reflect.Field("a", super.getPacket(), String.class).setValue(URL);
		this.URL = URL;
	}
	public void setHash(String hash) {
		Reflect.Field("b", super.getPacket(), String.class).setValue(hash);
		this.hash = hash;
	}
}

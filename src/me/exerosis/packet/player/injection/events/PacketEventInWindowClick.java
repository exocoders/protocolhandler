package me.exerosis.packet.player.injection.events;

import me.exerosis.packet.player.injection.packet.player.PacketPlayer;
import me.exerosis.packet.reflection.Reflect;

public class PacketEventInWindowClick extends PacketEvent{

	private int windowId;
	private int slot;
	private int button;
	private short actionNumber;
	private Object item;
	private int mode;

	public PacketEventInWindowClick(Object packet, PacketPlayer player) {
		super(packet, player);

		windowId = Reflect.Field("a", packet, int.class).getValue();

		slot = Reflect.Field("slot", packet, int.class).getValue();

		button = Reflect.Field("button", packet, int.class).getValue();

		actionNumber = Reflect.Field("d", packet, short.class).getValue();

		item = Reflect.Field("item", packet, Object.class).getValue();

		mode = Reflect.Field("shift", packet, int.class).getValue();
	}

	public int getWindowId() {
		return windowId;
	}

	public int getSlot() {
		return slot;
	}

	public int getButton() {
		return button;
	}

	public short getActionNumber() {
		return actionNumber;
	}

	public Object getItem() {
		return item;
	}

	public int getMode() {
		return mode;
	}

	public void setSlot(int slot) {
		this.slot = slot;
		Reflect.Field("slot", super.getPacket(), int.class).setValue(slot);
	}

	public void setItem(Object item) {
		this.item = item;
		Reflect.Field("item", super.getPacket(), Object.class).setValue(item);
	}

	public void setButton(int button) {
		this.button = button;
		Reflect.Field("button", super.getPacket(), int.class).setValue(button);
	}

	public void setMode(int mode) {
		this.mode = mode;
		Reflect.Field("shift", super.getPacket(), int.class).setValue(mode);
	}

}
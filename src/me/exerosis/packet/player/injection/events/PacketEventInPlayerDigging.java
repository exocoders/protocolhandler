package me.exerosis.packet.player.injection.events;

import me.exerosis.packet.player.injection.packet.player.PacketPlayer;
import me.exerosis.packet.reflection.Reflect;

public class PacketEventInPlayerDigging extends PacketEvent{ 
	private byte status;
	private Object blockPosition;
	private byte face;

	public PacketEventInPlayerDigging(Object packet, PacketPlayer player) {
		super(packet, player);
		status = Reflect.Field("a", packet, byte.class).getValue();
		blockPosition = Reflect.Field("b", packet, Object.class).getValue();
		face = Reflect.Field("c", packet, byte.class).getValue();
	}

	public void setStatus(byte status) {
		this.status = status;
		Reflect.Field("a", super.getPacket(), byte.class).setValue(status);
	}
	public void setLocation(Object location) {
		this.blockPosition = location;
		Reflect.Field("b", super.getPacket(), Object.class).setValue(location);
	}
	public void setFace(byte face) {
		this.face = face;
		Reflect.Field("c", super.getPacket(), byte.class).setValue(face);
	}
	public Object getBlockPosition() {
		return blockPosition;
	}
	public byte getFace() {
		return face;
	}
	public byte getStatus() {
		return status;
	}
}

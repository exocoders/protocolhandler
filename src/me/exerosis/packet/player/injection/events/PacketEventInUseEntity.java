package me.exerosis.packet.player.injection.events;

import me.exerosis.packet.player.injection.packet.player.PacketPlayer;
import me.exerosis.packet.reflection.Reflect;
import net.minecraft.server.v1_8_R1.EnumEntityUseAction;
import net.minecraft.server.v1_8_R1.Vec3D;

public class PacketEventInUseEntity extends PacketEvent{
	private int targetId;
	private Object action;
	private Object vector;

	public PacketEventInUseEntity(Object packet, PacketPlayer player) {
		super(packet, player, false);
		targetId = Reflect.Field("a", packet, int.class).getValue();
		action =  Reflect.Field("action", packet, Object.class).getValue();
		vector = Reflect.Field("c", packet, Object.class).getValue();
	}
	
	public void setTargetId(int targetId) {
		Reflect.Field("a", getPacket(), int.class).setValue(targetId);
		this.targetId = targetId;
	}
	public void setAction(EnumEntityUseAction action) {
		Reflect.Field("b", getPacket(), Object.class).setValue(action);
		this.action = action;
	}

	public void setVector(Vec3D vector) {
		Reflect.Field("c", getPacket(), Object.class).setValue(vector);
		this.vector = vector;
	}
	
	public int getTargetId() {
		return targetId;
	}
	public Object getVector() {
		return vector;
	}
	public Object getAction() {
		return action;
	}
}

package me.exerosis.packet.player.injection.events;

import me.exerosis.packet.player.injection.packet.player.PacketPlayer;
import me.exerosis.packet.reflection.Reflect;

public class PacketEventInEntityAction extends PacketEvent{ 
	private int entityId;
	private int actionId;
	private int jumpBoost;

	public PacketEventInEntityAction(Object packet, PacketPlayer player) {
		super(packet, player);
		entityId = Reflect.Field("a", packet, int.class).getValue();
		actionId = Reflect.Field("b", packet, int.class).getValue();
		jumpBoost = Reflect.Field("c", packet, int.class).getValue();
	}

	public int getEntityId() {
		return entityId;
	}
	public int getActionId() {
		return actionId;
	}
	public int getJumpBoost() {
		return jumpBoost;
	}
	public void setEntityId(int entityId) {
		this.entityId = entityId;
		Reflect.Field("a", super.getPacket(), int.class).setValue(entityId);
	}
	public void setActionId(int actionId) {
		this.actionId = actionId;
		Reflect.Field("b", super.getPacket(), int.class).setValue(actionId);
	}
	public void setJumpBoost(int jumpBoost) {
		this.jumpBoost = jumpBoost;
		Reflect.Field("c", super.getPacket(), int.class).setValue(jumpBoost);
	}
}

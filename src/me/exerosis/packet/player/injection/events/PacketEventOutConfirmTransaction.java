package me.exerosis.packet.player.injection.events;

import me.exerosis.packet.player.injection.packet.player.PacketPlayer;
import me.exerosis.packet.reflection.Reflect;

public class PacketEventOutConfirmTransaction extends PacketEvent{
	private int windowId;
	private short actionNumber;
	private boolean accepted;

	public PacketEventOutConfirmTransaction(Object packet, PacketPlayer player) {
		super(packet, player);
		windowId = Reflect.Field("a", packet, int.class).getValue();
		actionNumber = Reflect.Field("b", packet, short.class).getValue();
		accepted = Reflect.Field("c", packet, boolean.class).getValue();
	}
	public int getWindowId() {
		return windowId;
	}
	public short getActionNumber() {
		return actionNumber;
	}
	public boolean isAccepted(){
		return accepted;
	}

	public void setWindowId(int windowId) {
		Reflect.Field("a", super.getPacket(), int.class).setValue(windowId);
		this.windowId = windowId;
	}
	public void setActionNumber(short actionNumber) {
		Reflect.Field("b", super.getPacket(), short.class).setValue(actionNumber);
		this.actionNumber = actionNumber;
	}
	public void setAccepted(boolean accepted) {
		Reflect.Field("c", super.getPacket(), boolean.class).setValue(accepted);
		this.accepted = accepted;
	}
}

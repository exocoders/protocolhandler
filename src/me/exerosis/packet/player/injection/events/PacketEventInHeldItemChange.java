package me.exerosis.packet.player.injection.events;

import me.exerosis.packet.player.injection.packet.player.PacketPlayer;
import me.exerosis.packet.reflection.Reflect;

public class PacketEventInHeldItemChange extends PacketEvent{
	private short slot;
	
	public PacketEventInHeldItemChange(Object packet, PacketPlayer player) {
		super(packet, player);
		slot = Reflect.Field("a", packet, short.class).getValue();
	}
	
	public short getSlot() {
		return slot;
	}
	public void setSlot(short slot) {
		Reflect.Field("a", super.getPacket(), short.class).setValue(slot);
		this.slot = slot;
	}
}

package me.exerosis.packet.player.injection.events;

import java.util.concurrent.Callable;

import org.bukkit.Bukkit;

public class PacketEventFireTask implements Callable<PacketEvent>{
	private PacketEvent event;
	public PacketEventFireTask(PacketEvent event) {
		this.event = event;
	}
	public PacketEvent call() throws Exception {
		Bukkit.getPluginManager().callEvent(event);
		return event;
	}
}

package me.exerosis.packet.player.injection.events;

import me.exerosis.packet.player.injection.packet.player.PacketPlayer;
import me.exerosis.packet.reflection.Reflect;

import org.bukkit.inventory.ItemStack;

public class PacketEventOutEntityEquipment extends PacketEvent {
	private int EID;
	private int slot;
	private ItemStack item;

	public PacketEventOutEntityEquipment(Object packet, PacketPlayer player) {
		super(packet, player);
		Object nmsItem = Reflect.Field("c", packet, Object.class).getValue();
		
		EID = Reflect.Field("a", packet, int.class).getValue();
		slot = Reflect.Field("b", packet, int.class).getValue();
		item = (ItemStack) Reflect.Class("{cb}.inventory.CraftItemStack").getMethod("asBukkitCopy").call(nmsItem);
	}

	public void setEID(int EID) {
		this.EID = EID;
		Reflect.Field("a", super.getPacket(), int.class).setValue(EID);
	}
	public void setSlot(int slot) {
		this.slot = slot;
		Reflect.Field("b", super.getPacket(), int.class).setValue(slot);
	}
	public void setItem(ItemStack item) {
		this.item = item;
		Reflect.Field("c", super.getPacket(), Object.class).setValue(item);
	}

	public int getEID() {
		return EID;
	}
	public int getSlot() {
		return slot;
	}
	public ItemStack getItem() {
		return item;
	}
}

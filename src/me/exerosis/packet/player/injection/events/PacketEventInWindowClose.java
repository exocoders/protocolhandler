package me.exerosis.packet.player.injection.events;

import me.exerosis.packet.player.injection.packet.player.PacketPlayer;
import me.exerosis.packet.reflection.Reflect;
import net.minecraft.server.v1_8_R1.PacketPlayInCloseWindow;

public final class PacketEventInWindowClose extends PacketEvent{
	private int windowId;

	public PacketEventInWindowClose(PacketPlayInCloseWindow packet, PacketPlayer player) {
		super(packet, player);
		windowId = Reflect.Field("id", packet, int.class).getValue();
	}
	public int getWindowId() {
		return windowId;
	}
	public void setWindowId(int windowId) {
		this.windowId = windowId;
		Reflect.Field("id", super.getPacket(), int.class).setValue(windowId);
	}
}

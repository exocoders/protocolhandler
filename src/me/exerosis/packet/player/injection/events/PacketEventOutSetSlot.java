package me.exerosis.packet.player.injection.events;

import org.bukkit.inventory.ItemStack;

import me.exerosis.packet.player.injection.packet.player.PacketPlayer;
import me.exerosis.packet.reflection.Reflect;	

public class PacketEventOutSetSlot extends PacketEvent {
	private int windowID;
	private int slot;
	private ItemStack item;

	public PacketEventOutSetSlot(Object packet, PacketPlayer player) {
		super(packet, player);
		Object nmsItem = Reflect.Field("c", packet, Object.class).getValue();

		windowID = Reflect.Field("a", packet, int.class).getValue();
		slot = Reflect.Field("b", packet, int.class).getValue();
		item = (ItemStack) Reflect.Class("{cb}.inventory.CraftItemStack").getMethod("asBukkitCopy").call(nmsItem);
	}

	public void setWindowID(int windowID) {
		this.windowID = windowID;
		Reflect.Field("a", super.getPacket(), int.class).setValue(windowID);
	}
	public void setSlot(int slot) {
		this.slot = slot;
		Reflect.Field("b", super.getPacket(), int.class).setValue(slot);
	}
	public void setItem(ItemStack item) {
		this.item = item;
		Reflect.Field("c", super.getPacket(), Object.class).setValue(item);
	}

	public int getWindowID() {
		return windowID;
	}
	public int getSlot() {
		return slot;
	}
	public ItemStack getItem() {
		return item;
	}
}

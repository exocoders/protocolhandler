package me.exerosis.packet.player.injection.events;

import me.exerosis.packet.player.injection.packet.player.PacketPlayer;
import me.exerosis.packet.reflection.Reflect;

public class PacketEventOutUpdateTime extends PacketEvent {

	private long worldAge;
	private long timeOfDay;

	public PacketEventOutUpdateTime(Object packet, PacketPlayer player) {
		super(packet, player);
		worldAge = Reflect.Field("a", packet, long.class).getValue();
		timeOfDay = Reflect.Field("b", packet, long.class).getValue();
	}
	
	public void setWorldAge(long worldAge) {
		this.worldAge = worldAge;
		Reflect.Field("a", super.getPacket(), long.class).setValue(worldAge);
	}
	public void setTimeOfDay(long timeOfDay) {
		this.timeOfDay = timeOfDay;
		Reflect.Field("b", super.getPacket(), long.class).setValue(timeOfDay);
	}

	public long getTimeOfDay() {
		return timeOfDay;
	}
	public long getWorldAge() {
		return worldAge;
	}
}

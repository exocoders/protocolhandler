package me.exerosis.packet.player.injection.events;

import me.exerosis.packet.player.injection.packet.player.PacketPlayer;
import me.exerosis.packet.reflection.Reflect;
import net.minecraft.server.v1_8_R1.PacketPlayInChat;

public final class PacketEventInChat extends PacketEvent {
	private String message;

	public PacketEventInChat(PacketPlayInChat packet, PacketPlayer player) {
		super(packet, player, false);
		message = (String) Reflect.Field("a", packet, String.class).getValue();
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
		Reflect.Field("a", super.getPacket(), String.class).setValue(message);
	}
}

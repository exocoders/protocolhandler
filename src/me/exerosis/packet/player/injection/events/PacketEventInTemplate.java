package me.exerosis.packet.player.injection.events;

import org.bukkit.entity.Player;

public class PacketEventInTemplate extends PacketEvent{

	protected PacketEventInTemplate(PacketEvent packet, Player player) {
		super(packet, player);
		windowId = (int) PacketUtil.getPrivateField(packet, "a");
	}
}
